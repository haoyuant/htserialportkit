﻿using System;
using System.Diagnostics;
using DryIoc;
using HTToolkit.SerialPort;

namespace DotnetTester
{
    public class Controller
    {
        IContainer container;
        IHTSerialPort serialPort;

        public Controller(IContainer container)
        {
            this.container = container;
        }

        public void Execute()
        {
            var ports = container.Resolve<IHTSerialPort>().GetAvailablePorts();
            Debug.WriteLine("available ports:");
            foreach (var port in ports)
            {
                Console.WriteLine(port.Path);
                if (port.Path == "/dev/cu.usbmodem1411")
                {
                    serialPort = port;
                }
            }

            if (serialPort != null)
            {
                Console.WriteLine();
                Console.WriteLine();

                serialPort.BaudRate = 9600;
                serialPort.DataBits = 8;
                serialPort.StopBits = 1;
                //serialPort.Parity = ORSSerialPortParity.ORSSerialPortParityNone;
                serialPort.RTS = true;
                serialPort.DTR = true;
                serialPort.DataReceived += SerialPort_DataReceived;
                serialPort.Open();
            }
        }

        void SerialPort_DataReceived(object sender, byte[] data)
        {
            var rx = System.Text.Encoding.UTF8.GetString(data);
            Debug.WriteLine(rx);
        }

        public void SendMessageCmd()
        {
            var message = "Hello";
            serialPort.SendData(System.Text.Encoding.UTF8.GetBytes(message));
            Debug.WriteLine("sent message: " + message);
        }
    }
}
