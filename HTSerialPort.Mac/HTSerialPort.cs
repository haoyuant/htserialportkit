﻿using System;
using ORSSerialPortBinding;
using System.Linq;
using HTToolkit.SerialPort;
using Foundation;
using System.Collections.Generic;
using HTToolkit.PacketBuffer;
using System.Threading.Tasks;

namespace HTToolkit.SerialPort.Mac
{
    public class HTSerialPort : IHTSerialPort
    {
        ORSSerialPort serialPort;
        public ORSSerialPort SerialPort
        {
            get => serialPort;
            set
            {
                serialPort = value;
                //serialPort.SerialPortDidReceiveData += SerialPort_SerialPortDidReceiveData;
                serialPort.SerialPortWasOpened += SerialPort_SerialPortWasOpened;
                serialPort.SerialPortWasClosed += SerialPort_SerialPortWasClosed;
            }
        }

        PacketBufferBase packetBuffer;
        
        public string Path => SerialPort.Path;

        public string Name => SerialPort.Name;

        public bool IsOpen => SerialPort.IsOpen;

        public uint BaudRate { get => SerialPort.BaudRate; set => SerialPort.BaudRate = value; }
        public uint StopBits { get => SerialPort.StopBits; set => SerialPort.StopBits = value; }
        public uint DataBits { get => SerialPort.DataBits; set => SerialPort.DataBits = value; }
        public bool RTS { get => SerialPort.RTS; set => SerialPort.RTS = value; }
        public bool DTR { get => SerialPort.DTR; set => SerialPort.DTR = value; }

        public bool CTS => SerialPort.CTS;

        public bool DSR => SerialPort.DSR;

        public bool DCD => SerialPort.DCD;

        public HTSerialPort()
        {
        }

        void SerialPort_SerialPortDidReceiveData(object sender, SerialPortDidReceiveDataEventArgs e)
        {
            RaisedDataReceived(e.Data.ToArray());
        }


        public event EventHandler<byte[]> DataReceived;

        static public IHTSerialPort[] GetAvailablePorts()
        {
            var ports = ORSSerialPortManager.Shared.AvailablePorts.Select((port) =>
            {
                return new HTSerialPort
                {
                    SerialPort = port
                };
            });
            return ports.ToArray();
        }



        IHTSerialPort[] IHTSerialPort.GetAvailablePorts()
        {
            return GetAvailablePorts();
        }

        public void RaisedDataReceived(byte[] data)
        {
            packetBuffer?.Accept(data);
        }

        public void SendData(byte[] data)
        {
            SerialPort.SendData(NSData.FromArray(data));
        }

        void PacketBuffer_PacketReceived(object sender, byte[] e)
        {
            DataReceived?.Invoke(this, e);
        }

        TaskCompletionSource<bool> openTask;
        TaskCompletionSource<bool> closeTask;


        public Task<bool> Open(PacketBufferBase packetBuffer)
        {
            SerialPort.SerialPortDidReceiveData -= SerialPort_SerialPortDidReceiveData;
            SerialPort.SerialPortDidReceiveData += SerialPort_SerialPortDidReceiveData;
            this.packetBuffer = packetBuffer;
            packetBuffer.PacketReceived += PacketBuffer_PacketReceived;

            openTask = new TaskCompletionSource<bool>();
            if (SerialPort.IsOpen)
            {
                openTask.TrySetResult(true);
            }
            else
            {
                SerialPort.Open();
                Task.Run(async () =>
                {
                    await Task.Delay(1000);
                    if (!openTask.Task.IsCompleted)
                        openTask.TrySetResult(false);
                });
            }
            return openTask.Task;
        }

        public Task<bool> Close()
        {
            SerialPort.SerialPortDidReceiveData -= SerialPort_SerialPortDidReceiveData;

            if (packetBuffer != null)
            {
                packetBuffer.PacketReceived -= PacketBuffer_PacketReceived;
                packetBuffer = null;
            }

            closeTask = new TaskCompletionSource<bool>();
            if (!serialPort.IsOpen)
            {
                closeTask.TrySetResult(true);
            }
            else
            {
                SerialPort.Close();
                Task.Run(async () =>
                {
                    await Task.Delay(1000);
                    if (!closeTask.Task.IsCompleted)
                        closeTask.TrySetResult(false);
                });
            }

            return closeTask.Task;
        }

        void SerialPort_SerialPortWasOpened(object sender, EventArgs e)
        {
            if (openTask == null)
                //openTask = new TaskCompletionSource<bool>();
                return;
            openTask.TrySetResult(true);
        }

        void SerialPort_SerialPortWasClosed(object sender, EventArgs e)
        {
            if (closeTask == null)
                //closeTask = new TaskCompletionSource<bool>();
                return;
            closeTask.TrySetResult(true);
        }

    }
}
