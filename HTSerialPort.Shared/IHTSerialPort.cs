﻿using System;
using System.Threading.Tasks;
using HTToolkit.PacketBuffer;

namespace HTToolkit.SerialPort
{
    public interface IHTSerialPort
    {
        string Path { get; }
        string Name { get; }
        bool IsOpen { get; }
        bool CTS { get; }
        bool DSR { get; }
        bool DCD { get; }
        uint BaudRate { get; set; }
        uint StopBits { get; set; }
        uint DataBits { get; set; }
        bool RTS { get; set; }
        bool DTR { get; set; }

        Task<bool> Open(PacketBufferBase packetBuffer);
        Task<bool> Close();
        void SendData(byte[] data);
        IHTSerialPort[] GetAvailablePorts();

        event EventHandler<byte[]> DataReceived;
        void RaisedDataReceived(byte[] data);
    }
}
