﻿using System;
using System.IO.Ports;
using SysSerialPort = System.IO.Ports.SerialPort;
using HTToolkit.SerialPort;
using System.Collections.Generic;
using HTToolkit.PacketBuffer;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HTToolkit.SerialPort.Win32
{
    public class HTSerialPort : IHTSerialPort
    {
        SysSerialPort serialPort;
        public SysSerialPort SerialPort
        {
            get => serialPort;
            set
            {
                serialPort = value;
                //serialPort.DataReceived += SerialPort_DataReceived;
            }
        }

        PacketBufferBase packetBuffer;

        public string Path => SerialPort.PortName;

        public string Name => SerialPort.PortName;

        public bool IsOpen => SerialPort.IsOpen;

        public bool CTS => SerialPort.CtsHolding;

        public bool DSR => SerialPort.DsrHolding;

        public bool DCD => SerialPort.CDHolding;

        public uint BaudRate { get => (uint)SerialPort.BaudRate; set => SerialPort.BaudRate = (int)value; }
        public uint StopBits { get => (uint)SerialPort.StopBits; set => SerialPort.StopBits = (StopBits)value; }
        public uint DataBits { get => (uint)SerialPort.DataBits; set => SerialPort.DataBits = (int)value; }
        public bool RTS { get => SerialPort.RtsEnable; set => SerialPort.RtsEnable = value; }
        public bool DTR { get => SerialPort.DtrEnable; set => SerialPort.DtrEnable = value; }

        public event EventHandler<byte[]> DataReceived;


        public IHTSerialPort[] GetAvailablePorts()
        {
            var htPorts = new List<IHTSerialPort>();
            var portNames = SysSerialPort.GetPortNames();
            foreach (var portName in portNames)
            {
                var port = new SysSerialPort();
                port.PortName = portName;
                var htPort = new HTSerialPort
                {
                    SerialPort = port
                };
                htPorts.Add(htPort);
            }
            return htPorts.ToArray();
        }



        TaskCompletionSource<bool> openTask;
        TaskCompletionSource<bool> closeTask;


        public Task<bool> Open(PacketBufferBase packetBuffer)
        {
            SerialPort.DataReceived -= SerialPort_DataReceived;
            SerialPort.DataReceived += SerialPort_DataReceived;
            this.packetBuffer = packetBuffer;
            packetBuffer.PacketReceived += PacketBuffer_PacketReceived;
            openTask = new TaskCompletionSource<bool>();
            try
            {
                SerialPort.Open();
                openTask.TrySetResult(true);
            }
            catch (Exception e)
            {
                openTask.TrySetResult(false);
                Debug.WriteLine(e.StackTrace);
            }

            return openTask.Task;
        }

        public Task<bool> Close()
        {
            closeTask = new TaskCompletionSource<bool>();
            SerialPort.DataReceived -= SerialPort_DataReceived;
            packetBuffer.PacketReceived -= PacketBuffer_PacketReceived;
            packetBuffer = null;
            try
            {
                SerialPort.Close();
                closeTask.TrySetResult(true);
            }
            catch (System.IO.IOException e)
            {
                closeTask.TrySetResult(false);
                Debug.WriteLine(e.StackTrace);
            }

            return closeTask.Task;
        }


        void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var dataLength = SerialPort.BytesToRead;
            if (dataLength > 0)
            {
                var data = new byte[dataLength];
                SerialPort.Read(data, 0, dataLength);
                RaisedDataReceived(data);
            }
        }

        public void RaisedDataReceived(byte[] data)
        {
            packetBuffer?.Accept(data);
        }

        public void SendData(byte[] data)
        {
            SerialPort.Write(data, 0, data.Length);
        }

        private void PacketBuffer_PacketReceived(object sender, byte[] packet)
        {
            DataReceived?.Invoke(this, packet);
        }
    }
}
