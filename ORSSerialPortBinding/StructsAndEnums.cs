﻿using System;

namespace ORSSerialPortBinding
{
    public enum ORSSerialPortParity : uint
    {
        ORSSerialPortParityNone = 0,
        ORSSerialPortParityOdd,
        ORSSerialPortParityEven
    }
}
