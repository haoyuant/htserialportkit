﻿using System;
using System.Diagnostics;
using AppKit;
using Foundation;
using HTToolkit.SerialPort;
using HTToolkit.SerialPort.Mac;
using DotnetTester;
using DryIoc;

namespace Tester
{
    public partial class ViewController : NSViewController
    {
        DotnetTester.Controller controller;
        IContainer container;
        public ViewController(IntPtr handle) : base(handle)
        {
            container = new Container();
            container.Register<IHTSerialPort, HTSerialPort>(Reuse.Singleton);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Do any additional setup after loading the view.
            controller = new Controller(container);
            controller.Execute();
            //var portManager = ORSSerialPortManager.Shared;
            //var ports = portManager.AvailablePorts;
            //var ports = container.Resolve<IHTSerialPort>().GetAvailablePorts();
            //Debug.WriteLine("available ports:");
            //foreach (var port in ports)
            //{
            //    Console.WriteLine(port.Path);
            //    if (port.Path == "/dev/cu.usbmodem1411")
            //    {
            //        serialPort = port;
            //    }
            //}

            //if (serialPort != null)
            //{
            //    Console.WriteLine();
            //    Console.WriteLine();

            //    serialPort.BaudRate = 9600;
            //    serialPort.DataBits = 8;
            //    serialPort.StopBits = 1;
            //    //serialPort.Parity = ORSSerialPortParity.ORSSerialPortParityNone;
            //    serialPort.RTS = true;
            //    serialPort.DTR = true;
            //    serialPort.DataReceived += SerialPort_DataReceived;
            //    serialPort.Open();
            //}
        }

        partial void sendMessage(NSObject sender)
        {
            controller.SendMessageCmd();
            //var message = new NSString("Hello.");
            //serialPort.SendData(message.Encode(NSStringEncoding.UTF8).ToArray());
            //Debug.WriteLine("sent message: " + message);
        }


        //IHTSerialPort serialPort = null;

        //void SerialPort_DataReceived(object sender, byte[] data)
        //{
        //    var rx = System.Text.Encoding.UTF8.GetString(data);
        //    Debug.WriteLine(rx);
        //}


        //ORSSerialPort serialPort = null;

        //void SerialPort_SerialPortDidReceiveData(object sender, SerialPortDidReceiveDataEventArgs e)
        //{
        //    var rx = NSString.FromData(e.Data, NSStringEncoding.UTF8);
        //    Console.WriteLine(rx);
        //}


        //void Port_SerialPortWasRemovedFromSystem(object sender, EventArgs e)
        //{
        //    serialPort = null;
        //}

        //void SerialPort_SerialPortWasOpened(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Serial port {0} was opened.", ((ORSSerialPort)sender).Path);
        //}

        //void SerialPort_SerialPortWasClosed(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Serial port {0} was closed.", ((ORSSerialPort)sender).Path);
        //}


        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }
    }
}
